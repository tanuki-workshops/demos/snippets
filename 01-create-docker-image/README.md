> Prequisites: create a subgroup

1. Create `mocha` project
2. Add `Dockerfile`
3. Add `.gitlab-ci.yml`

Then: commit, build

- Show the Registry
- Talk about the Kaniko cache

### Refs:

- https://docs.gitlab.com/ee/ci/docker/using_docker_build.html 
- https://docs.gitlab.com/ee/ci/docker/using_kaniko.html


