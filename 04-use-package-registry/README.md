# Use the GitLab Package Registry

- Create a project `awesome-app`
- Copy the id of the project
- Create locally a directory with a file to upload

## Upload

```bash
PROJECT_ID=26229047

curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" \
     --upload-file awesome-app.js \
     "https://gitlab.com/api/v4/projects/${PROJECT_ID}/packages/generic/awesome_package/0.0.1/awesome-app-release.js"
```

## Download

```bash
curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" \
     "https://gitlab.com/api/v4/projects/${PROJECT_ID}/packages/generic/awesome_package/0.0.1/awesome-app-release.js" \
     --output awesome-app-release.js
```


## Refs

- https://docs.gitlab.com/ee/user/packages/generic_packages/
