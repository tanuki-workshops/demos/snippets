
- Use the `awesome-app` project
- Add the `.gitlab-ci.yml` *(explain)*
- Commit
- Create a **tag**
- Show pipelines list
- Show release

## Refs

- https://gitlab.com/gitlab-org/release-cli/-/blob/master/docs/index.md 
