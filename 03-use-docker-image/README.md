# Use the created Docker image in another project

- Create a `use-mocha` project
- Add `tests.js` file
- Add `.gitlab-cy.yml` file
  - 🖐️ dont forget to update te path of the image if necessary
- Commit, Run


