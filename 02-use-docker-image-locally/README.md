# Use the GitLab registry locally

## Pre-requisites

- Start the Docker engine (locally on the laptop)

## Setup

- Create a directory (eg: `use-docker-image`)
- Create a sub directory `workspace`
- Copy the `tests.js` file to `use-docker-image/workspace`

## Connect and pull

```bash
# login with GitLab user + token 
DOCKER_REGISTRY="registry.gitlab.com"
echo "$GITLAB_TOKEN_ADMIN" | docker login $DOCKER_REGISTRY --username $GITLAB_ADMIN --password-stdin
docker pull registry.gitlab.com/tanuki-workshops/demos/sandbox/mocha:latest

docker image ls
docker image tag registry.gitlab.com/tanuki-workshops/demos/sandbox/mocha:latest gitlab/mocha:latest
```

## Execute tests

```bash
cd use-docker-image # 🖐️ it's important to be in the right place
docker rm use-mocha
docker run -v $(pwd)/workspace:/workspace --name use-mocha -i -t gitlab/mocha mocha /workspace/tests.js 
```

## Remove docker image

```bash
docker image rm gitlab/mocha
docker image rm registry.gitlab.com/tanuki-workshops/demos/sandbox/mocha 
```

